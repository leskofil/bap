# Automated storage model
Model combines previous work from [ESO-Nav](https://github.com/EnviloN/ozobot-mapf-simulator) and MAPD instance solver [Token Passing](https://github.com/Lodz97/Multi-Agent_Pickup_and_Delivery)
to create model of automated warehouse. Implementation offers either MAPF simulation 
and storage simulation. To simulate real agents robot [Ozobot Evo](https://shop.ozobot.com/products/evo-classroom-kit-2-0-12-bots-1?_pos=14&_sid=8a927b492&_ss=r)
was used

Example runs with Ozobots can be seen in the following playlist [Automated Storage Model](https://youtube.com/playlist?list=PLott6eT-TXuM-x-nwfF16fATie3WGiawd)

## Set-up

### Set-up simulator
1. Python virtual environment:
    - Create a new python virtual environment \
      `python3 -m venv .env`
    - Activate the virtual environment \
     `source .env/bin/activate` - Linux \
      `.\venv\Scripts\activate` - Windows
2. Install required packages:
    - `pip3 install -r requirements.txt`
3. *Optional* - Configure screen parameters:
    - Modify or add screen configuration based on your display in \
      `./resources/config/simulation.ini`

### Set-up BoOX solver (for MAPF simulation only)
1. Build the **boOX** program:
    - `git clone https://github.com/surynek/boOX.git` - clone boOX repo
    - `cd boox/boOX-y/` - Go to *boox* folder
    - `make` - Build the program
    - Check and configure the path to the program in `./resources/config/simulation.ini` (also see section *Simulator Configuration*)


## Simulator Configuration
The simulation is configurable and all the configurable parameters can be found in is in `./resources/config/simulation.ini`. 

### Section [ozobots]
Configuration of outputs for Ozobot robots.
- `line_width` - Width of the following line in millimeters
- `wall_width` - Width of the wall line in millimeters
- `tile_border_width` - Width of the tile border line in millimeters
- `tile_size` - Size of the tile in millimeters
- `color_code_radius` - Radius of the Color Code circle in millimeters
- `intersection_width` - Width of the intersection indicator for colored paths


### Section [solver] (for MAPF simulation only)
Path to the boOX main folder, where the executables are. Also selection of solver and algorithm.
- `path` - Path to the boOX `src/main` folder, where the solver executables are. 
- `solver` - Specified solver (`mapf_solver_boOX` or `rota_solver_boOX`)
- `algorithm` - Algorithm used for solving (One from: `cbs`, `cbs+`, `cbs++`, `smtcbs`, `smtcbs+`, `smtcbs++`)

### Section [simulator]
Configuration of the simulation animations.
- `agent_type` - Agent type implementation. Can be:
    - `dummy` - Full path (ESO-OzoNav 0)
    - `animated` - Animated path with sharp turns (ESO-OzoNav 1)
    - `ozobot` - Animated paths with curved turns, Color Codes, and ability to be colored (ESO-OzoNav 2 & 3)
- `step_time` - Time in milliseconds that should take Ozobot to go between tiles
- `tail_lag` - Time lag of the tail (effectively its length)
- `display_borders` - Flag, if tile borders should be displayed
- `display_walls` - Flag, if walls should be displayed
- `direction_preview` - Flag, if direction arrow indicator should be displayed
- `colors` - Flag, if the paths should be colored (Only for `ozobot` agent implementation, also displays intersection indicators)

## Command-line arguments

### Applied to both storage and MAPF simulators 
- `-r <w h>`, `--resolution <w h>` - Resolution of the application window [default: `1920 1080`]
- `-f`, `--full-screen` - Starts application in full-screen (ignores `-r <w h>` if used)

### Run storage
- `-m <map_file>` - (required) relative path to the map file from `./resources/maps/storage_maps`
- `-s` - (required) Runs storage simulator
- `-s -e` - Runs storage editor

### Run MAPF (with boOX set up)
- `-c <cfg_file>`, `--config-file <cfg_file>` - relative path to the display configuration file from `./resources/config/simulation.ini`
- `-m <map_file>` - (required) relative path to the map file from `./resources/maps/mapf_maps`
- `-ma <w h a>`, `--map-attributes <w h a>` - (required if attributes are not in the map name) map attributes [width, height, number of agents]
- `-e`, `--editor` - Runs map editor instead of simulator
- `-d`, `--debug` - Runs debug mode (more logging in `./resources/logs/log.log`)

## Usage
To run the simulation follow these steps: \
Go to the `./storage` folder \
`cd ./storage`

### Storage simulator
To start storage simulator \
`python3 run.py -s -m <map_file> [optional_arguments]`

To start storage editor \
`python3 run.py -s -e [optional_arguments]`

### MAPF simulator 
To start in MAPF simulator mode. \
`python3 run.py -c <cfg_file> -m <map_file> [optional_arguments]`

To start in MAPF map editor mode. \
`python3 run.py -e -c <cfg_file> [optional_arguments]`
