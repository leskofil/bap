.. Automated Storage Model documentation master file, created by
   sphinx-quickstart on Wed Jan  4 09:59:44 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Automated Storage Model's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: storage

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
