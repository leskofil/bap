import enum
import logging
import os
import re
from os.path import isfile, join

import pygame
import yaml
from pygame.locals import *

from graphics.drawables import FillRect, Rect, FillChecker
from graphics.shapes import Point, Rectangle
from map_editor.EditorException import EditorException
from map_editor.editor import Editor
from utils.constants import Colors, Directions
from utils.constants import Values
from utils.helper import get_root


class StorageEditor(Editor):
    def __init__(self, config, ozomap):
        super().__init__(config, ozomap)
        self.agent_starts = []
        self.pickup = []
        self.delivery = []
        self.endpoint = []
        self.mode = Modes.WALL
        self.modes = {i.name: i.value for i in Modes}

    def run(self):
        """ Method run the storage editor
        """
        logging.info("Starting the Map Editor process.")
        self._greet_user(Values.EDITOR_NAME)
        self._get_map_attributes_from_user()
        self.ozomap.init_empty_map(self.config)
        self._init_screen()
        self.__draw_map()

        save = self.__game_loop()
        pygame.quit()

        if save:
            self.__save_map()
        logging.info("Successfully finished the Map Editor process.")

    def __save_map(self):
        map_name = self.__get_map_name_from_user()
        self.__save_to_file(map_name)

    def __save_to_file(self, map_name):
        mapd = {'random': True,
                'n_tasks': 10,
                'agents': [],
                'map': {'walls': [], 'non_task_endpoints': [], 'pickup': [], 'delivery': []},
                'delays': {},
                'distribution': {'name': 'exponential',
                                 'params': {'scale': 5
                                            }
                                 }
                }
        mapd['map']['dimensions'] = [self.config.map_width, self.config.map_height]
        for id, tile in enumerate(self.agent_starts):
            a = {'start': [tile.x_pos, tile.y_pos], 'name': 'agent{}'.format(id)}
            mapd['agents'].append(a)
            mapd['delays']['agent{}'.format(id)] = []
            mapd['map']['non_task_endpoints'].append([tile.x_pos, tile.y_pos])
        for id, tile in enumerate(self.endpoint):
            e = [tile.x_pos, tile.y_pos]
            mapd['map']['non_task_endpoints'].append(e)
        for id, tile in enumerate(self.pickup):
            p = [tile.x_pos, tile.y_pos]
            mapd['map']['pickup'].append(p)
        for id, tile in enumerate(self.delivery):
            d = [tile.x_pos, tile.y_pos]
            mapd['map']['delivery'].append(d)

        for tile in self.ozomap.map_tile_generator():
            if tile.has_wall(Directions.DOWN) and tile.y_pos < self.config.map_height - 1:
                w = {'locationA': [tile.x_pos, tile.y_pos], 'locationB': [tile.x_pos, tile.y_pos + 1]}
                mapd['map']['walls'].append(w)
            if tile.has_wall(Directions.RIGHT) and tile.x_pos < self.config.map_width - 1:
                w = {'locationA': [tile.x_pos, tile.y_pos], 'locationB': [tile.x_pos + 1, tile.y_pos]}
                mapd['map']['walls'].append(w)

        with open(Values.STORAGE_MAPS_PATH + map_name, "w") as file:
            d = yaml.dump(mapd, default_flow_style=None)
            file.write(d)

    def __get_map_name_from_user(self):
        name = ""
        valid = False
        while not valid:
            automatic_name = self.__generate_automatic_map_name()
            name = input("Enter new level name [{}]: ".format(automatic_name))
            name = automatic_name if name == "" else name
            if os.path.isfile(Values.STORAGE_MAPS_PATH + name + '.yaml'):
                print("A level with this name already exists in '{}'!".format(Values.MAPF_MAPS_PATH))
                continue
            else:
                valid = True
        return name

    def __generate_automatic_map_name(self):
        mypath = join(get_root(), Values.STORAGE_MAPS_PATH)
        onlyfiles = [f for f in os.listdir(mypath) if isfile(join(mypath, f))]
        onlyfiles = sorted(onlyfiles)
        last_num = re.findall('[0-9]+', onlyfiles[-1])
        last_num = int(last_num[0]) + 1
        name = 's_{}.yaml'.format(last_num)
        return name

    def __game_loop(self):
        end, save = False, False
        while not end:
            end, save = self.__handle_events(end, save)
        return save

    def __handle_events(self, end, save):
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                logging.info("Quitting Map Editor.")
                end = True
            if event.type == KEYDOWN and event.key == K_SPACE:
                end, save = True, True

            if event.type == KEYDOWN and event.key == K_p:
                self.mode = Modes.PICKUP
            if event.type == KEYDOWN and event.key == K_d:
                self.mode = Modes.DELIVERY
            if event.type == KEYDOWN and event.key == K_e:
                self.mode = Modes.ENDPOINT
            if event.type == KEYDOWN and event.key == K_s:
                self.mode = Modes.AGENT_START
            if event.type == KEYDOWN and event.key == K_w:
                self.mode = Modes.WALL
            if event.type == MOUSEBUTTONUP:
                x, y = pygame.mouse.get_pos()
                self.__handle_mouse_click(Point(x, y))
        return end, save

    def __handle_mouse_click(self, pos):
        if self.mode == Modes.WALL:
            self._handle_wall_toggle(pos)
        elif self.mode.name in self.modes:
            self.__handle_tile_toggle(pos)
        else:
            raise EditorException("Invalid editor mode!")

        self.__draw_map()

    def __handle_tile_toggle(self, pos):
        tile = self._get_tile_from_position(pos)
        if tile is not None:
            if self.mode == Modes.PICKUP:
                self.__handle_tile_pickup_toggle(tile)
            if self.mode == Modes.DELIVERY:
                self.__handle_tile_delivery_toggle(tile)
            if self.mode == Modes.ENDPOINT:
                self.__handle_tile_endpoint_toggle(tile)
            if self.mode == Modes.AGENT_START:
                self.__handle_tile_agent_start_toggle(tile)

    def __handle_tile_pickup_toggle(self, tile):
        if tile.pickup > 0:
            tile.pickup = 0
            self.pickup.remove(tile)
        else:
            tile.pickup = 1
            self.pickup.append(tile)

    def __handle_tile_delivery_toggle(self, tile):
        if tile.delivery > 0:
            tile.delivery = 0
            self.delivery.remove(tile)
        else:
            tile.delivery = 1
            self.delivery.append(tile)

    def __handle_tile_endpoint_toggle(self, tile):
        if tile.endpoint == 1:
            tile.endpoint = 0
            self.endpoint.remove(tile)
        else:
            tile.endpoint = 1
            self.endpoint.append(tile)

    def __handle_tile_agent_start_toggle(self, tile):
        if tile.endpoint > 1:
            tile.endpoint = 0
            self.agent_starts.remove(tile)
        else:
            tile.endpoint = 2
            self.agent_starts.append(tile)

    def __draw_map(self):
        self._screen.fill(Colors.WHITE)
        self.__draw_tiles()
        self._draw_walls()
        self._update()

    def __draw_tiles(self):
        for tile in self.ozomap.grid.tile_generator():
            self.__draw_tile(tile)

    def __draw_tile(self, tile):
        tile_size = self.config.tile_size + 1  # This needs to be done for tile borders to overlap during drawing
        rectangle = Rectangle(Point(tile.origin.x, tile.origin.y), tile_size, tile_size)
        if tile.pickup > 0 and tile.delivery > 0:
            FillChecker(rectangle, Colors.START, Colors.FINISH).draw(self._screen)
            self._render_text_in_tile(tile, "P / D")
        elif tile.endpoint == 1:
            FillRect(rectangle, Colors.START).draw(self._screen)
            self._render_text_in_tile(tile, "E")
        elif tile.endpoint > 1:
            FillRect(rectangle, Colors.START).draw(self._screen)
            self._render_text_in_tile(tile, "S / E")
        elif tile.delivery > 0:
            FillRect(rectangle, Colors.FINISH).draw(self._screen)
            self._render_text_in_tile(tile, "D")
        elif tile.pickup > 0:
            FillRect(rectangle, Colors.PICKUP).draw(self._screen)
            self._render_text_in_tile(tile, "P")

        Rect(rectangle, self.config.tile_border_width, Colors.GREY).draw(self._screen)


class Modes(enum.Enum):
    PICKUP = 1
    DELIVERY = 2
    ENDPOINT = 3
    AGENT_START = 4
    WALL = 5
    TASK = 6
