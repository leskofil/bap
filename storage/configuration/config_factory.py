import logging
import os

from configuration.cli_options import CLIOptions
from configuration.config_options import ConfigOptions
from configuration.configuration import EditorConfig, StorageConfig, SimulatorConfig
from utils.constants import Values
from utils.helper import get_root


def read_config():
    """ Method creates config for application from cli parameters and config file

    :return: Application config
    """
    options = CLIOptions().parse()
    if not options.debug:
        logging.getLogger().setLevel(logging.INFO)

    config_path = os.path.join(get_root(), Values.SIMULATOR_CONFIG)
    config = ConfigOptions(config_path).parse()

    if options.editor:
        config_merge = EditorConfig(options, config)
        return config_merge
    elif options.storage:
        config_merge = StorageConfig(options, config)
        return config_merge
    else:
        config_merge = SimulatorConfig(options, config)
        return config_merge
