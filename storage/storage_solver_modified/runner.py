import json
import os

import yaml

from storage_solver_modified import RoothPath
from storage_solver_modified.Simulation.CBS.cbs import Location, Wall
from storage_solver_modified.Simulation.TP_with_recovery import TokenPassingRecovery
from storage_solver_modified.Simulation.simulation_new_recovery import SimulationNewRecovery


def run_tp(input_file, tasks=None):
    input_file = os.path.join(input_file)
    output_file = os.path.join(RoothPath.get_root(), 'output.yaml')

    # Read from input_file file
    with open(input_file, 'r') as param_file:
        try:
            param = yaml.load(param_file, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)

    if tasks is None:
        tasks = param['tasks']

    dimensions = param['map']['dimensions']
    walls = param["map"]["walls"] if param["map"]["walls"] is not None else []
    wall_set = set()
    for o in walls:
        locA = Location(o["locationA"][0], o["locationA"][1])
        locB = Location(o["locationB"][0], o["locationB"][1])
        wall_set.add(Wall(locA, locB))
        wall_set.add(Wall(locB, locA))

    non_task_endpoints = param['map']['non_task_endpoints']
    agents = param['agents']
    delays = param['delays']
    param['tasks'] = tasks
    param['delays'] = delays

    simulation = SimulationNewRecovery(tasks, agents, delays=delays)
    tp = TokenPassingRecovery(agents, dimensions, wall_set, non_task_endpoints, simulation, a_star_max_iter=4000, k=1,
                              replan_every_k_delays=False, pd=0.0, p_max=1, p_iter=1, new_recovery=True)

    while tp.get_completed_tasks() != len(tasks):
        simulation.time_forward(tp)

    cost = 0
    for path in simulation.actual_paths.values():
        cost = cost + len(path)
    output = {'schedule': simulation.actual_paths,
              'cost': cost,
              'makespan': simulation.time,
              'completed_tasks_times': tp.get_completed_tasks_times(),
              'n_replans': tp.get_n_replans()}
    with open(output_file, 'w') as output_yaml:
        yaml.safe_dump(output, output_yaml)


if __name__ == '__main__':
    with open(os.path.join(RoothPath.get_root(), 'config.json'), 'r') as json_file:
        config = json.load(json_file)
    input_file = os.path.join(RoothPath.get_root(), os.path.join(config['input_path'], config['input_name']))
    output_file = os.path.join(RoothPath.get_root(), 'output.yaml')

    run_tp(input_file)
