from graphics.drawables import DrawableGroup, FillChecker, FillRect
from graphics.ozomap_view.ozomap_drawable import OzomapDrawableParser
from graphics.shapes import Rectangle, Point
from level.ozomap_storage import OzoMapStorage
from utils.constants import Colors


class OzoStorageDrawable(OzomapDrawableParser):
    def __init__(self, ozomap: OzoMapStorage, config):
        super().__init__(ozomap, config)

    def parse(self):
        drawables = [self.__positions_to_drawable(), self._borders_to_drawable(), self._walls_to_drawable()]
        return drawables

    def __positions_to_drawable(self):
        group = DrawableGroup()
        for tile in self.ozomap.map_tile_generator():
            rectangle = Rectangle(Point(tile.origin.x, tile.origin.y), self.config.tile_size, self.config.tile_size)
            if tile.delivery and tile.pickup:
                group.add_drawable(FillChecker(rectangle, Colors.FINISH, Colors.PICKUP))
            if tile.endpoint:
                group.add_drawable(FillRect(rectangle, Colors.START))
            elif tile.delivery:
                group.add_drawable(FillRect(rectangle, Colors.FINISH))
            elif tile.pickup:
                group.add_drawable(FillRect(rectangle, Colors.PICKUP))
        return group
