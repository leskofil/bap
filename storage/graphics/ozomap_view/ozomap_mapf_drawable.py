from graphics.drawables import DrawableGroup, FillChecker, FillRect
from graphics.ozomap_view.ozomap_drawable import OzomapDrawableParser
from graphics.shapes import Rectangle, Point
from utils.constants import Colors


class OzoMapfDrawable(OzomapDrawableParser):
    def __init__(self, ozomap, config):
        super().__init__(ozomap, config)

    def parse(self):
        drawables = [self.__positions_to_drawable(), self._borders_to_drawable(), self._walls_to_drawable()]
        return drawables

    def __positions_to_drawable(self):
        group = DrawableGroup()
        for tile in self.ozomap.map_tile_generator():
            rectangle = Rectangle(Point(tile.origin.x, tile.origin.y), self.config.tile_size, self.config.tile_size)
            if tile.agent_start > 0 and tile.agent_finish > 0:
                group.add_drawable(FillChecker(rectangle, Colors.START, Colors.FINISH))
            elif tile.agent_start > 0:
                group.add_drawable(FillRect(rectangle, Colors.START))
            elif tile.agent_finish > 0:
                group.add_drawable(FillRect(rectangle, Colors.FINISH))
        return group
