import logging
import os.path

from mapf_solvers.static_solvers import MapfSolverBoOX, StorageSolver
from utils.constants import Values
from utils.helper import get_root


def init_solver(config, ozomap):
    """Function initializes the solver instance with given arguments."""
    if config.storage:
        solver = StorageSolver(config.map_path, os.path.join(get_root(), Values.STORAGE_SOLVER_OUTPUT_PATH), ozomap)
    else:
        solver_args = {"input-file": config.map_path, "algorithm": config.solver_algorithm}
        solver = MapfSolverBoOX(config.solver_path + config.solver, solver_args)

    # solver = ManualSolver()  # Use in case the plan needs to be modified
    logging.info("Solver initialized.")
    return solver
