import logging

from configuration.config_factory import read_config
from graphics.ozomap_view.ozomap_mapf_drawable import OzoMapfDrawable
from graphics.ozomap_view.ozomap_storage_drawable import OzoStorageDrawable
from level.ozomap import OzoMap
from level.ozomap_mapf import OzoMapMapf
from level.ozomap_storage import OzoMapStorage
from map_editor.editor import Editor
from map_editor.storage_editor import StorageEditor
from simulator.mapf_simulator import MapfSimulator
from simulator.storage_simulator import StorageSimulator
from utils.constants import Values
from utils.helper import windows_scale_fix


def clear_log(log_file: str) -> None:
    f = open(log_file, 'r+')
    f.truncate(0)


def init_logger():
    clear_log(Values.LOGS_PATH)
    logging.basicConfig(filename=Values.LOGS_PATH, format='%(asctime)s - %(levelname)s: %(message)s',
                        level=logging.DEBUG)


def configure_application():
    """ Method creates simulation or editor based on cli parameters

    :return: Simulation or editor
    """
    config_merge = read_config()

    if config_merge.editor:
        if config_merge.storage:
            ozomap = OzoMapStorage(config_merge)
            return StorageEditor(config_merge, ozomap)
        else:
            ozomap = OzoMapMapf(config_merge)
            return Editor(config_merge, ozomap)
    elif config_merge.storage:
        ozomap = OzoMapStorage(config_merge).load_map_yaml(config_merge)
        map_drawable = OzoStorageDrawable(ozomap, config_merge).parse()
        return StorageSimulator(config_merge, ozomap, map_drawable)
    else:
        ozomap = OzoMapMapf(config_merge).load_map(config_merge)
        map_drawable = OzoMapfDrawable(ozomap, config_merge).parse()
        return MapfSimulator(config_merge, ozomap, map_drawable)


if __name__ == '__main__':
    init_logger()
    windows_scale_fix()
    application = configure_application()
    application.run()

    logging.info("----------------------------------------------------------------------------------------")
