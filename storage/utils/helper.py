import ctypes
import os
from pathlib import Path


def get_root():
    """ Method returns path to storage directory
    :return: Path to storage dir
    :rtype: str
    """

    path = Path(os.path.dirname(os.path.abspath(__file__)))
    return path.parent.absolute()


def windows_scale_fix():
    """ Method fixes windows scale for pygame
    """
    try:
        ctypes.windll.user32.SetProcessDPIAware()
    except AttributeError:
        pass  # Windows XP doesn't support monitor scaling, so just do nothing.


if __name__ == '__main__':
    print(get_root())
