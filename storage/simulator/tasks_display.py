import pygame.display

from configuration.config_factory import read_config
from level.tasks.task import Task


class TasksDisplay:
    """
     Class handles tasks displaying on monitor
    """
    def __init__(self, config, inactive_tasks, finished_tasks):
        self.menu_top = None
        self.main_rect = None
        self.config = config
        self.inactive_tasks = self.__format_tasks(inactive_tasks, finished_tasks)
        self.finished_tasks = []
        self.active_tasks = []
        self.tasks_to_display = {'inactive_tasks': self.inactive_tasks,
                                 'active_tasks': self.active_tasks,
                                 'finished_tasks': self.finished_tasks}

        self.height = 200
        self.width = self.config.window_width - self.config.left_margin * 2
        self.space = 10
        self.padding = 8

        self.font = pygame.font.Font(None, 35)
        self.display_surface = pygame.display.get_surface()
        self.__setup()

    def __setup(self):
        """"""
        self.menu_top = self.config.simulator_window_height
        width = self.config.window_width - self.config.left_margin * 2
        self.main_rect = pygame.Rect(self.config.left_margin, self.menu_top, width, 200)

    def __show_entry(self, text_surf, top):
        bq_rect = pygame.Rect(self.main_rect.left, top, self.width, text_surf.get_height() + self.padding * 2)
        pygame.draw.rect(self.display_surface, 'White', bq_rect, 0, 4)

        text_rect = text_surf.get_rect(midleft=(self.main_rect.left + 20, bq_rect.centery))
        self.display_surface.blit(text_surf, text_rect)

    def preview(self):
        """
        Method show tasks at the start of simulation
        """
        text_surfs = self.__generate_text_surf()

        for text_id, text_surf in enumerate(text_surfs):
            top = self.main_rect.top + text_id * (text_surf.get_height() + (self.padding * 2) + self.space)
            self.__show_entry(text_surf, top)

    def update(self, time):
        """ Method updates tasks displayed on monitor
        :param time: Time of simulation
        """
        self.__update_tasks(time)
        text_surfs = self.__generate_text_surf()

        for text_id, text_surf in enumerate(text_surfs):
            top = self.main_rect.top + text_id * (text_surf.get_height() + (self.padding * 2) + self.space)
            self.__show_entry(text_surf, top)

    def __generate_text_surf(self):
        text_surfs = []
        for k, v in self.tasks_to_display.items():
            text = ''
            if len(v) < 10 and len(v) != 0:
                for item in v[:-1]:
                    text += str(item) + ', '
                text += str(v[-1])
                text_surf = self.font.render(k + ' : ' + text, True, 'Black')
                text_surfs.append(text_surf)
            elif len(v) >= 10:
                for item in v[:10]:
                    text += str(item) + ', '
                text += ' ...'
                text_surf = self.font.render(k + ' : ' + text, True, 'Black')
                text_surfs.append(text_surf)
            else:
                text_surf = self.font.render(k + ' : ' + '[]', True, 'Black')
                text_surfs.append(text_surf)
        return text_surfs

    def __update_tasks(self, time):
        self.tasks_to_display['active_tasks'] += [item for item in self.tasks_to_display['inactive_tasks'] if
                                                  time >= item.start_time]
        self.tasks_to_display['finished_tasks'] += [item for item in self.tasks_to_display['active_tasks'] if
                                                    time >= item.delivery_time]

        self.tasks_to_display['inactive_tasks'] = [item for item in self.tasks_to_display['inactive_tasks'] if
                                                   time < item.start_time]
        self.tasks_to_display['active_tasks'] = [item for item in self.tasks_to_display['active_tasks'] if
                                                 time < item.delivery_time]

    def __format_tasks(self, inactive_tasks, finished_tasks):
        merged_inactive_tasks = []
        for k, v in inactive_tasks.items():
            v['start_time'] = v[
                                  'start_time'] * self.config.step_time - self.config.step_time - self.config.step_time / 2
            finished_tasks[k] = finished_tasks[
                                    k] * self.config.step_time - self.config.step_time - self.config.step_time / 2
            t = Task(v['start'], v['goal'], v['start_time'], finished_tasks[k], k)
            merged_inactive_tasks.append(t)
        merged_inactive_tasks = sorted(merged_inactive_tasks, key=lambda t: t.start_time, reverse=True)
        return merged_inactive_tasks


if __name__ == '__main__':
    print('aa')
    config = read_config()
    finished_tasks = {'task0': 11, 'task1': 7}
    inactive_tasks = {'task0': {'start_time': 0, 'start': [2, 1], 'goal': [3, 1], 'task_name': 'task0'},
                      'task1': {'start_time': 2, 'start': [2, 1], 'goal': [3, 1], 'task_name': 'task1'}}

    a = []
    for k, v in inactive_tasks.items():
        t = Task(v['start'], v['goal'], v['start_time'], finished_tasks[k])
        a.append(t)
    a = sorted(a, key=lambda t: t.start_time, reverse=True)

    for k, v in inactive_tasks.items():
        inactive_tasks[k]['start_time'] = inactive_tasks[k]['start_time'] * config.step_time - config.step_time / 2
    for k, v in finished_tasks.items():
        finished_tasks[k] = finished_tasks[k] * config.step_time - config.step_time / 2
    TasksDisplay(config, inactive_tasks, finished_tasks)
