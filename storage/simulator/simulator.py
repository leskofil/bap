import logging
import sys

import pygame

from utils.constants import Colors, Values
from utils.timer import Timer


class Simulator:
    def __init__(self, config):
        self.ozomap = None
        self.map_objects = None
        self.agents = None
        self.config = config
        self.timer = Timer()
        # self.timer = Timer(True)  # Debug mode timer
        self.__pygame_init()

    def _init_agents(self):
        pass

    def __pygame_init(self):
        logging.info("Initializing pygame.")
        pygame.init()
        pygame.display.set_caption(Values.APP_NAME)
        self._screen = None
        self.__width, self.__height = self.config.window_width, self.config.window_height

    def _init_screen(self):
        logging.info("Initializing screen.")

        if self.config.fullscreen:
            self._screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        else:
            self._screen = pygame.display.set_mode([self.config.window_width, self.config.window_height])
        self._screen.fill(Colors.WHITE)

        self.__width, self.__height = pygame.display.get_surface().get_size()
        logging.debug("Application window resolution: {} x {} (px)".format(self.__width, self.__height))

    def run(self):
        logging.info("Starting the Simulator process.")

        self._preview_map()
        self._wait_for_user()

        self.timer.start(self._get_longest_path_time())
        while not self.timer.is_finished():
            self._handle_events()
            time = self.timer.get_time()

            self._update_agents(time)
            self._draw_map()._draw_active_paths()
            self._update()

        self._wait_for_user()

        pygame.quit()
        logging.info("Successfully finished the Simulator process.")

    @staticmethod
    def _wait_for_user():
        while True:
            for event in pygame.event.get():
                if (event.type == pygame.QUIT) or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                    logging.info("Quitting application.")
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    return

    @staticmethod
    def _handle_events():
        for event in pygame.event.get():
            if (event.type == pygame.QUIT) or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                logging.info("Quitting application.")
                pygame.quit()
                sys.exit()

    def _draw_map(self):
        self._screen.fill(Colors.WHITE)

        self.map_objects[0].draw(self._screen)  # Agent Starts/Ends

        if self.config.display_grid:
            self.map_objects[1].draw(self._screen)  # Grid border lines

        if self.config.display_walls:
            self.map_objects[2].draw(self._screen)  # Walls

        return self

    def __draw_all_paths(self):
        for agent in self.agents:
            self.__draw_agent_path(agent)

    def __draw_agent_path(self, agent):
        for drawable in agent.get_active_path():
            drawable.draw(self._screen)

    def _update(self):
        pygame.display.update()
        return self

    def _preview_map(self):
        self._draw_map()

        # if self.config.direction_preview:
        #     for agent in self.agents:
        #         if agent.direction_arrow is not None:
        #             agent.direction_arrow.draw(self.__screen)

        self._update()

    def _update_agents(self, time):
        for agent in self.agents:
            agent.update_path(time)
        return self

    def _draw_active_paths(self):
        for agent in self.agents:
            agent.get_active_path().draw(self._screen)
        return self

    def _get_longest_path_time(self):
        max_len = 0
        for agent in self.agents:
            p_len = agent.plan_length
            if p_len > max_len:
                max_len = p_len
        return (max_len * self.config.step_time) + self.config.tail_lag
