from configuration.config_factory import read_config
from level.ozomap_storage import OzoMapStorage
from mapf_solvers.solver_factory import init_solver
from simulator.simulator import Simulator


class MapfSimulator(Simulator):
    def __init__(self, config, ozomap, drawables):
        super().__init__(config)
        self.ozomap = ozomap
        self.solver = init_solver(config, self.ozomap)
        self.plans = self.solver.plan()
        self.map_objects = drawables  # To draw map
        self.agents = self._init_agents()

    def _init_agents(self):
        agents = []
        for agent_id in self.plans:
            agents.append(
                self.config.agent_class(agent_id, self.ozomap, self.config).update_plans(self.plans[agent_id]))

        return agents


if __name__ == '__main__':
    conf = read_config()
    test_map = OzoMapStorage(conf).load_map_yaml(conf)
    MapfSimulator(conf, test_map).run()
    print("test")
