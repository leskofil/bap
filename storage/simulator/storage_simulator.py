import logging

import pygame

from mapf_solvers.solver_factory import init_solver
from simulator.simulator import Simulator
from simulator.tasks_display import TasksDisplay


class StorageSimulator(Simulator):
    def __init__(self, config, ozomap, map_drawable):
        super().__init__(config)
        self.ozomap = ozomap
        self.solver = init_solver(config, self.ozomap)
        self.plans, self.finished_tasks = self.solver.plan()
        self.map_objects = map_drawable
        self.agents = self._init_agents()
        self.td = None

    def _init_agents(self):
        agents = []
        for agent_id in self.plans:
            agents.append(
                self.config.agent_class(agent_id, self.ozomap, self.config).update_plans(self.plans[agent_id]))

        return agents

    def _init_screen(self):
        Simulator._init_screen(self)
        self.td = TasksDisplay(self.config, self.ozomap.tasks, self.finished_tasks)

    def _preview_map(self):
        self._draw_map()
        self.td.preview()
        if self.config.direction_preview:
            for agent in self.agents:
                if agent.direction_arrow is not None:
                    agent.direction_arrow.draw(self._screen)

        self._update()

    def run(self):
        """ Method runs storage simulation
        """
        logging.info("Starting the Simulator process.")

        self._init_screen()
        self._preview_map()
        self._wait_for_user()
        self.timer.start(self._get_longest_path_time())
        while not self.timer.is_finished():
            self._handle_events()
            time = self.timer.get_time()

            self._update_agents(time)
            self._draw_map()._draw_active_paths()
            self.td.update(time)
            self._update()

        self._wait_for_user()

        pygame.quit()
        logging.info("Successfully finished the Simulator process.")

    def _draw_map(self):
        Simulator._draw_map(self)

        return self
