import re

from level.grid.mapf_grid import MapfGrid
from level.ozomap import OzoMap


class OzoMapMapf(OzoMap):
    def __init__(self, config):
        super().__init__()
        self.grid = MapfGrid(config)

    def __init_tiles(self, tiles):
        """Method initializes all level tiles.

        All four walls are built (set to True) around the tile and if there is a start/end point for any agent
        on the tile, these values are updated.

        Args:
            tiles (list[str]): Lines from level file that contain tiles (graph vertices)
        """
        for tile in tiles:
            tile_id, start, end = map(int, re.compile("\((\d+),(\d+),(\d+)\)").match(tile).groups())
            x, y = OzoMap.get_position_from_id(self, tile_id)
            self.grid.get_tile(x, y).agent_start = start
            self.grid.get_tile(x, y).agent_finish = end
            self.grid.get_tile(x, y).build_all_walls()

    def load_map(self, config):
        edges, tiles = OzoMap.load_map(self, config)
        self.__init_tiles(tiles)
        OzoMap._destroy_walls(self, edges)
        return self
