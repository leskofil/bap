from level.grid.grid import Grid
from level.grid.storage_tile import StorageMapfTile


class StorageGrid(Grid):
    def __init__(self, config):
        super().__init__(config)
        self._tiles = self._tiles = [[StorageMapfTile() for _ in range(config.max_map_width)] for _ in
                                     range(config.max_map_height)]

        for col in range(len(self._tiles[0])):
            for row in range(len(self._tiles)):
                tile_origin = self._origin.moved(col * self._tile_size, row * self._tile_size)
                self._tiles[row][col] = StorageMapfTile(tile_origin, col, row, config.tile_size)
