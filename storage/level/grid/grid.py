class Grid:
    def __init__(self, config):
        self._origin = config.map_origin
        self._tile_size = config.tile_size
        self.width, self.height = config.max_map_width, config.max_map_height

        self._tiles = [[]]

    def get_tile(self, x, y):
        return self._tiles[y][x]

    def tile_generator(self):
        for col in range(len(self._tiles[0])):
            for row in range(len(self._tiles)):
                yield self._tiles[row][col]

    def get_origin(self):
        return self._origin
