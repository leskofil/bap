from graphics.shapes import Point
from level.ozomap_exception import OzoMapException
from utils.constants import Directions


class Tile:
    def __init__(self, origin=Point(0, 0), x_pos=0, y_pos=0, size=0):
        self.origin, self.x_pos, self.y_pos = origin, x_pos, y_pos
        self._walls = [False] * 4  # [upper, right, bottom, left]
        self._size = size

    def has_wall(self, direction):
        """Getter for wall in direction.

        Args:
            direction (Directions): Direction from middle

        Returns:
            bool: Flag if wall is activated
        """
        return self._walls[direction]

    def build_all_walls(self):
        """Sets all walls to True."""
        self._walls = [True] * 4

    def build_wall(self, direction):
        """Sets wall in direction to True.

        Args:
            direction (Directions): Direction from middle
        """
        self._walls[direction] = True

    def destroy_all_walls(self):
        """Sets all walls to False."""
        self._walls = [False] * 4

    def destroy_wall(self, direction):
        """Sets wall in direction to False.

        Args:
            direction (Directions): Direction from middle
        """
        self._walls[direction] = False

    def toggle_wall(self, direction):
        """Sets wall in direction to the opposite value.

        Args:
            direction (Directions): Direction from middle
        """
        self._walls[direction] = not self._walls[direction]

    def direction_to(self, other):
        x, y = self.origin.to_list()
        x_other, y_other = other.origin.to_list()
        if x != x_other and y != y_other:
            raise OzoMapException("Diagonal directions are not supported.")
        elif x == x_other and y == y_other:
            return Directions.NONE
        elif x == x_other:
            return Directions.DOWN if y - y_other < 0 else Directions.UP
        elif y == y_other:
            return Directions.RIGHT if x - x_other < 0 else Directions.LEFT

    def get_middle(self):
        half_size = round(self._size / 2)
        return self.origin.moved(half_size, half_size)

    def get_edge_middle(self, direction):
        half_size = round(self._size / 2)
        if direction == Directions.NONE:
            return self.get_middle()
        elif direction == Directions.UP:
            return self.origin.moved(half_size, 0)
        elif direction == Directions.RIGHT:
            return self.origin.moved(self._size, half_size)
        elif direction == Directions.DOWN:
            return self.origin.moved(half_size, self._size)
        elif direction == Directions.LEFT:
            return self.origin.moved(0, half_size)
