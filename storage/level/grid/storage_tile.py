from graphics.shapes import Point
from level.grid.tile import Tile


class StorageMapfTile(Tile):
    def __init__(self, origin=Point(0, 0), x_pos=0, y_pos=0, size=0):
        super().__init__(origin, x_pos, y_pos, size)
        self.pickup = 0
        self.delivery = 0
        self.endpoint = 0
