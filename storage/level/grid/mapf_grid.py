from level.grid.grid import Grid
from level.grid.mapftile import MapfTile


class MapfGrid(Grid):
    def __init__(self, config):
        super().__init__(config)
        self._tiles = [[MapfTile() for _ in range(config.max_map_width)] for _ in
                       range(config.max_map_height)]

        for col in range(len(self._tiles[0])):
            for row in range(len(self._tiles)):
                tile_origin = self._origin.moved(col * self._tile_size, row * self._tile_size)
                self._tiles[row][col] = MapfTile(tile_origin, col, row, config.tile_size)
