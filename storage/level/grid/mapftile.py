from graphics.shapes import Point
from level.grid.tile import Tile


class MapfTile(Tile):
    """Class represents a level tile.

    Attributes:
        origin (Point): Top-left point of the tile
        agent_start (int): ID of an agent that has starting point here (else 0)
        agent_finish (int): ID of an agent that has finish point here (else 0)
        __walls (list[bool]): Flags if there are walls around the tile (Format: [upper, right, bottom, left])
    """

    def __init__(self, origin=Point(0, 0), x_pos=0, y_pos=0, size=0):
        """Initialization of the Tile instance.

        Args:
            origin (Point): Top-left point of the tile
        """
        super().__init__(origin, x_pos, y_pos, size)
        self.agent_start = 0
        self.agent_finish = 0

    def is_start(self):
        """Returns true if there is an agent's start on the tile."""
        return True if self.agent_start > 0 else False

    def is_finish(self):
        """Returns true if there is an agent's finish on the tile."""
        return True if self.agent_finish > 0 else False
