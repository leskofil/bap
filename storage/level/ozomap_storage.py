import re

import yaml

from configuration.config_factory import read_config
from configuration.configuration import StorageConfig
from level.grid.storage_grid import StorageGrid
from level.ozomap import OzoMap
from level.tasks.task_manager import gen_tasks
from utils.constants import Directions


class OzoMapStorage(OzoMap):
    def __init__(self, config: StorageConfig):
        super().__init__()
        self.grid = StorageGrid(config)
        self.tasks = {}

    def __init_tiles(self, tiles):
        """Method initializes all level tiles.

        All four walls are built (set to True) around the tile and if there is a start/end point for any agent
        on the tile, these values are updated.

        Args:
            tiles (list[str]): Lines from level file that contain tiles (graph vertices)
        """
        for tile in tiles:
            tile_id, endpoint, pickup, delivery = map(int,
                                                      re.compile("\((\d+),(\d+),(\d+),(\d+)\)").match(tile).groups())
            x, y = OzoMap.get_position_from_id(self, tile_id)
            self.grid.get_tile(x, y).endpoint = endpoint
            self.grid.get_tile(x, y).pickup = pickup
            self.grid.get_tile(x, y).delivery = delivery
            self.grid.get_tile(x, y).build_all_walls()

    def load_map(self, config):
        """
         Method reads storage map from text file
        :param config: Storage Configuration
        :return: Storage instance
        :rtype: OzoMapStorage
        """
        edges, tiles = OzoMap.load_map(self, config)
        OzoMap._destroy_walls(self, edges)
        self.__init_tiles(tiles)
        return self

    def load_map_yaml(self, config):
        """
        Method reads storage map from yaml file
        :param config: Storage Configuration
        :return: Storage instance
        :rtype: OzoMapStorage
        """
        with open(config.map_path, 'r') as param_file:
            try:
                yaml_map = yaml.load(param_file, Loader=yaml.FullLoader)
            except yaml.YAMLError as exc:
                print(exc)

        if yaml_map['random']:
            self.tasks = self.__build_tasks_random(yaml_map)
            self.__build_pickup_and_delivery_from_yaml(yaml_map['map'])
        else:
            self.tasks = self.__parse_tasks(yaml_map)

        agents = yaml_map['agents']
        map = yaml_map['map']
        config.map_width, config.map_height, config.map_agent_count = map['dimensions'][0], map['dimensions'][1], len(
            agents)

        self.__build_map_from_yaml(agents, map, config)
        return self

    def __build_tasks_random(self, yaml_map):
        distribution = yaml_map['distribution']['name']
        starts = yaml_map['map']['pickup']
        ends = yaml_map['map']['delivery']
        n_tasks = yaml_map['n_tasks']
        dist_params = yaml_map['distribution']['params']
        return gen_tasks(distribution, starts, ends, n_tasks, dist_params)

    def __build_tasks_from_yaml(self):
        for task in self.tasks.values():
            x_start, y_start = task['start'][0], task['start'][1]
            x_goal, y_goal = task['goal'][0], task['goal'][1]
            self.grid.get_tile(x_start, y_start).pickup = True
            self.grid.get_tile(x_goal, y_goal).delivery = True

    def __build_map_from_yaml(self, agents, map, config):
        self.init_empty_map(config)
        wall = map['walls'] if map['walls'] is not None else []
        self.__build_walls_from_yaml(wall)
        self.__build_agent_starts_from_yaml(agents)
        self.__build_tasks_from_yaml()

    def __build_agent_starts_from_yaml(self, agents):
        for agent in agents:
            x, y = agent['start'][0], agent['start'][1]
            self.grid.get_tile(x, y).endpoint = True

    def __build_walls_from_yaml(self, walls):
        self.init_empty_map()
        for wall in walls:
            x_from, y_from = wall['locationA'][0], wall['locationA'][1]
            x_to, y_to = wall['locationB'][0], wall['locationB'][1]
            if x_from == x_to:
                if y_from < y_to:
                    self.grid.get_tile(x_from, y_from).build_wall(Directions.DOWN)
                    self.grid.get_tile(x_to, y_to).build_wall(Directions.UP)
                else:
                    self.grid.get_tile(x_from, y_from).build_wall(Directions.UP)
                    self.grid.get_tile(x_to, y_to).build_wall(Directions.DOWN)
            elif y_from == y_to:
                if x_from < x_to:
                    self.grid.get_tile(x_from, y_from).build_wall(Directions.RIGHT)
                    self.grid.get_tile(x_to, y_to).build_wall(Directions.LEFT)
                else:
                    self.grid.get_tile(x_from, y_from).build_wall(Directions.LEFT)
                    self.grid.get_tile(x_to, y_to).build_wall(Directions.RIGHT)

    def __parse_tasks(self, yaml_map):
        yaml_tasks = yaml_map['tasks']
        parsed_tasks = {}
        for t in yaml_tasks:
            parsed_tasks[t['task_name']] = t
        return parsed_tasks

    def __build_pickup_and_delivery_from_yaml(self, yaml_map):
        for p in yaml_map['pickup']:
            x_start, y_start = p[0], p[1]
            self.grid.get_tile(x_start, y_start).pickup = True
        for d in yaml_map['delivery']:
            x_goal, y_goal = d[0], d[1]
            self.grid.get_tile(x_goal, y_goal).delivery = True


if __name__ == '__main__':
    conf = read_config()
    ozomap = OzoMapStorage(conf).load_map_yaml(conf)
    print("test")
