import random

import numpy.random


def gen_unif(n_tasks, starts, ends, dist_params):
    """Method generates tasks for uniform distribution

    :param n_tasks: Number of tasks to be generated
    :param starts: List of start positions
    :param ends: List of end positions
    :param dist_params: Parameter of distribution
    :return: Dictionary of generated tasks
    """
    tasks = {}
    data = numpy.random.uniform(dist_params['min'], dist_params['max'], size=n_tasks).astype(int)
    data = data.clip(0)
    for i in range(n_tasks):
        tasks['task' + str(i)] = {'start_time': data[i], 'start': random.choice(starts), 'goal': random.choice(ends),
                                  'task_name': 'task' + str(i)}
    return tasks


def gen_exp(n_tasks, starts, ends, dist_params):
    """ Method generates tasks for exponential distribution
        :param n_tasks: Number of tasks to be generated
        :param starts: List of start positions
        :param ends: List of end positions
        :param dist_params: Parameter of distribution
        :return: Dictionary of generated tasks
        """
    tasks = {}
    data = numpy.random.exponential(dist_params['scale'], size=n_tasks).astype(int)
    for i in range(n_tasks):
        tasks['task' + str(i)] = {'start_time': data[i], 'start': random.choice(starts), 'goal': random.choice(ends),
                                  'task_name': 'task' + str(i)}
    return tasks


def gen_normal(n_tasks, starts, ends, dist_params):
    """ Method generates tasks for normal distribution
        :param n_tasks: Number of tasks to be generated
        :param starts: List of start positions
        :param ends: List of end positions
        :param dist_params: Parameter of distribution
        :return: Dictionary of generated tasks
        """
    tasks = {}
    data = numpy.random.normal(loc=dist_params['loc'], scale=dist_params['scale'], size=n_tasks).astype(int)
    data = data.clip(min=0)
    for i in range(n_tasks):
        tasks['task' + str(i)] = {'start_time': data[i], 'start': random.choice(starts), 'goal': random.choice(ends),
                                  'task_name': 'task' + str(i)}
    return tasks


def gen_tasks(distribution, starts, ends, n_tasks, dist_params):
    dict = {
        'uniform': gen_unif,
        'exponential': gen_exp,
        'normal': gen_normal,
    }
    return dict[distribution](n_tasks, starts, ends, dist_params)


import matplotlib.pyplot as plt

if __name__ == '__main__':
    data = numpy.random.exponential(1, size=20).astype(int)
    _ = plt.hist(data, color='black', bins=10)
    plt.xlabel('timestep')
    plt.ylabel('task count')
    plt.title(r'Distribution of tasks with exponential distribution: $\lambda=1$')
    plt.show()

    data = numpy.random.uniform(0, 10, size=20).astype(int)
    _ = plt.hist(data, color='black', bins=10)
    plt.xlabel('timestep')
    plt.ylabel('task count')
    plt.title(r'Distribution of tasks with uniform distribution: $\min=0$ $\max=10$')
    plt.show()

    data = numpy.random.normal(loc=5, scale=1, size=20).astype(int)
    _ = plt.hist(data, color='black', bins=10)
    plt.xlabel('timestep')
    plt.ylabel('task count')
    plt.title(r'Distribution of tasks with normal distribution: $\mu=5$ $\sigma=1$')
    plt.show()
