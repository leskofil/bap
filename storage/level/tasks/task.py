class Task:
    """
     Class represent tasks in storage simulator
    """
    def __init__(self, pickup_tile, delivery_tile, start_time, end_time, name):
        self.pickup_tile, self.delivery_tile, self.start_time, self.delivery_time = pickup_tile, delivery_tile, start_time, end_time
        self.ID = name

    def __str__(self):
        return self.ID
